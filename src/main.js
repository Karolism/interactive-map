import { createApp } from "vue";
import App from "./App.vue";
import "./styles/main.scss";
// import * as VueGoogleMaps from "vue2-google-maps";

const app = createApp(App);
// app.use(VueGoogleMaps, {
//   load: {
//     key: "AIzaSyBa9WGqwc28JuuYVW3GRpB3qme5DXHU2w4",
//     installComponents: true,
//   },
// });
app.mount("#app");
