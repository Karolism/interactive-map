## Install and run
Yarn:
``$ yarn && yarn dev``

NPM: ``$ npm install && npm run dev``

## Build

Yarn: ``$ yarn  build``

NPM: ``$ npm run build``


## Live preview

Yarn: ```yarn build && yarn serve```

NPM: ```npm run build && npm run serve```
