module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "black-550": "#404040",
        "blue-550": "#0054A6",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
